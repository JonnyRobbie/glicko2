# glicko2: An R package for computing glicko2 ratings using the Glicko-2 algorithm.

The glicko2 package privides two important functions:
glicko2 and glicko2.multi

## glicko2 function:
The glicko2 function allows you to recompute (or compute new) ratings using
only a single rating period.

## glicko2.multi function:
The glicko2.multi function allows you to compute new ratings using a
data.frame containing full scores from multiple training periods.

---
## Installation using R:

    install.packages("devtools")
    install_git("git://gitlab.com/JonnyRobbie/glicko2.git")

## Usage:

    games.multi <- data.frame(
      period = c(1, 1, 1, 1, 2, 2, 2, 2),
      playerA = c("foo", "bar", "baz", "foo", "baz", "cux", "foo", "bar"),
      playerB = c("bar", "cux", "cux", "baz", "foo", "bar", "baz", "baz"),
      result = c(1, 0, 1, 0.5, 0.67, 0, 1, 1))
    glicko2.multi(games.multi)
